<?php
/**
 * MWFewoManager
 *
 * @author Mirko Witzmann <info@mwfewomanager.de>
 * @copyright mwfewomanager.de
 */

namespace MWitzmann\Contao\MWFM\Ajax\Controller;


use Contao\System;
use MWFM\Backoffice\Controller\FrontendController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 *
 * @Route("/{locale}/mwfm/ajax", defaults={"_scope" = "frontend", "_token_check" = false})
 */
class AjaxController
{
    /**
     * Api entry point
     *
     * @param Request $request
     * @return Response
     *
     * @Route("/", name="mwfm_ajax_index")
     */
    public function indexAction(Request $request, $locale = 'en')
    {
        $getData = $request->query->all();
        $postData = $request->request->all();

        if (!empty($getData) && empty($postData)) {
            $postData = $getData;
        }

//        echo "<pre>GET "; print_r($getData); echo "</pre>";
//        echo "<pre>POST "; print_r($postData); echo "</pre>";

        \Contao\Controller::setStaticUrls();

        System::loadLanguageFile('default', $locale);
        System::loadLanguageFile('values', $locale);

        $encodings = $request->getEncodings();
        $controller = new FrontendController();
        $response = new Response($controller->request($postData, $locale));

//        $response->headers->set('Content-Type', 'text/html; charset=UTF-8');
//        $response->headers->set('Content-Type', 'application/json; charset=UTF-8');

        return $response;
    }
}