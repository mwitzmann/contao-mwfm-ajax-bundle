<?php
/**
 * MWFewoManager
 *
 * @author Mirko Witzmann <info@mwfewomanager.de>
 * @copyright mwfewomanager.de
 */

namespace MWitzmann\Contao\MWFM\Ajax;


use Symfony\Component\HttpKernel\Bundle\Bundle;

class ContaoMWFMAjaxBundle extends Bundle
{
}